"""Start a pipeline on a project and retrieve the archived artifacts."""
import os
import zipfile
from time import sleep

import requests


def start_pipeline(project_id, api_token, ref='master'):
    """Start the gitlab pipeline and return the json response if successful."""
    response = requests.request(
        method='POST',
        url=f'https://gitlab.com/api/v4/projects/{project_id}/'
            f'pipeline?ref={ref}',
        headers={'PRIVATE-TOKEN': api_token})
    if response.status_code == 201:
        return response.json()
    else:
        raise requests.HTTPError(response.json())


def wait_for_pipeline(project_id, pipeline_id, api_token, timeout=10800):
    """Wait for pipeline to succeed or fail (wait while running or pending)."""
    wait_time = 20
    while timeout > 0:
        pipeline = requests.request(
            method='GET',
            url=f'https://gitlab.com/api/v4/projects/'
                f'{project_id}/pipelines/{pipeline_id}',
            headers={'PRIVATE-TOKEN': api_token}).json()
        print(f'Pipeline {pipeline_id} current status is {pipeline["status"]}')
        if pipeline['status'] in ['pending', 'running']:
            sleep(wait_time)
            timeout -= wait_time
        else:
            break
    return pipeline


def retry_pipeline(project_id, pipeline_id, api_token):
    """Restart the specified pipeline."""
    response = requests.request(
        method='POST',
        url=f'https://gitlab.com/api/v4/projects/{project_id}/'
        f'pipelines/{pipeline_id}/retry',
        headers={'PRIVATE-TOKEN': api_token})
    if response.status_code == 201:
        return response.json()
    else:
        raise requests.HTTPError(response.json())


def get_pipeline_jobs(project_id, pipeline_id, api_token):
    """Get all the jobs associated with a pipeline."""
    return requests.request(
        method='GET',
        url=f'https://gitlab.com/api/v4/projects/{project_id}/'
            f'pipelines/{pipeline_id}/jobs',
        headers={
            'PRIVATE-TOKEN': api_token}).json()


def deploy_job_success(project_id, pipeline_id, deploy_job_name, api_token):
    """Return True if the job identified as the deploy job succeded."""
    jobs = get_pipeline_jobs(project_id, pipeline_id, api_token)
    for job in jobs:
        if job['name'] == deploy_job_name and job['status'] == 'success':
            return True
    return False


def get_job_artifacts(project_id, job_id, api_token):
    """Get the job artifacts and unzip them in the current directory."""
    response = requests.request(
        method='GET',
        url=f'https://gitlab.com/api/v4/projects/{project_id}/'
            f'jobs/{job_id}/artifacts',
        headers={'PRIVATE-TOKEN': api_token})
    if response.status_code == 200:
        zippedData = response.content
        filename = f'project-{project_id}-job-{job_id}-artifacts.zip'
        print(f'Fetched archive {filename}')
        output = open(filename, 'wb')
        output.write(zippedData)
        output.close()
        zipFileObject = zipfile.ZipFile(filename)
        for name in zipFileObject.namelist():
            uncompressed = zipFileObject.read(name)
            with open(name, 'wb') as file:
                file.write(uncompressed)
                print(f'Found and extracted {name} in {filename}')


if __name__ == '__main__':
    project_id = os.environ.get('PROJECT_ID')
    api_token = os.environ.get('GITLAB_API_TOKEN')
    deploy_job_name = os.environ.get('DEPLOY_JOB_NAME')
    pipeline = start_pipeline(project_id, api_token)
    pipeline = wait_for_pipeline(
        project_id=project_id,
        pipeline_id=pipeline['id'],
        api_token=api_token)
    if pipeline['status'] not in ['failed', 'success']:
        raise AssertionError(
            f'Pipeline had unexpected status {pipeline["status"]}')

    if not deploy_job_success(
            project_id,
            pipeline['id'],
            deploy_job_name,
            api_token):
        print(
            f'Deploy job {deploy_job_name} was not successfull, retrying...')
        retry_pipeline(project_id, pipeline['id'], api_token)
        pipeline = wait_for_pipeline(
            project_id=project_id,
            pipeline_id=pipeline['id'],
            api_token=api_token)

    jobs = get_pipeline_jobs(
        project_id=project_id,
        pipeline_id=pipeline['id'],
        api_token=api_token)

    for job in jobs:
        get_job_artifacts(project_id, job_id=job['id'], api_token=api_token)
