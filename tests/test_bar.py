"""Run some tests to generate result for report feature."""
import pytest

a = [1, 2, 3, 4, 5]
b = ['a', 'b', 'c', 'd']
c = ['wow', 'cow', 'amazing']

a_to_test = 1
b_to_test = 'c'
c_to_test = 'cow'


@pytest.fixture(params=a)
def afixture(request):
    """Foo."""
    if request.param == a_to_test:
        return request.param
    else:
        pytest.skip(f'Testing only {a_to_test}')


@pytest.fixture(params=b)
def bfixture(request):
    """Foo."""
    if request.param == b_to_test:
        return request.param
    else:
        pytest.skip(f'Testing only {b_to_test}')


@pytest.fixture(params=c)
def cfixture(request):
    """Foo."""
    if request.param == c_to_test:
        return request.param
    else:
        pytest.skip(f'Testing only {c_to_test}')


@pytest.fixture(params=c, scope='module')
def dfixture(request):
    """Foo."""
    if request.param == c_to_test:
        return request.param
    else:
        pytest.skip(f'Testing only {c_to_test}')


def test_skipping_from_fixture_bar(afixture, bfixture, cfixture):
    """Foo."""
    assert True


def test_skipping_from_module_fixture_bar(dfixture):
    """Foo."""
    assert True
