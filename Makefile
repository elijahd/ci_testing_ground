PYTEST_OPTIONS = -vvv

help:
	@echo "Please use \`make <target>' where <target> is one of:"
	@echo "  help              to show this message"
	@echo "  all               to to execute lint and test-coverage."
	@echo "  install           to install ci_testing_ground if only running tests."
	@echo "  install-dev       to install ci_testing_ground in editable mode to"
	@echo "                    develop test cases"
	@echo "  lint              to lint the source code"
	@echo "  test              to run ci_testing_ground's framework unit tests"

all: lint test

install:
	pip install .

install-dev:
	pip install -e .[dev]

lint:
	flake8 .

test:
	py.test -vvv tests

.PHONY: all install install-dev lint test
