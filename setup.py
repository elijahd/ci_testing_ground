#!/usr/bin/env python3
"""A setuptools-based script for installing ci_testing_ground."""
import os

from setuptools import find_packages, setup

_project_root = os.path.abspath(os.path.dirname(__file__))

# Get the long description from the README file
with open(os.path.join(_project_root, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='ci_testing_ground',
    author='Elijah DeLee',
    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Quality Assurance'
    ],
    description=(
        'A toy project for testing gitlab ci features'
    ),
    extras_require={
        'dev': [
            # For `make lint`
            'flake8',
            'flake8-docstrings',
            'flake8-import-order',
            'flake8-quotes',
        ],
    },
    install_requires=[
        'pytest',
        'requests',
    ],
    license='GPLv3',
    long_description=long_description,
    packages=find_packages(include=['*']),
    url='https://gitlab.com/kdelee/ci_testing_ground',
)
